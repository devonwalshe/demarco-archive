class Asset < ActiveRecord::Base

  ### Model Definition
  paginates_per 24
  ImageContentTypes = ["application/pdf", "image/jpeg", "image/pjpeg", "image/png", "image/x-png", "image/gif"]

  ### Scopes
  scope :published, -> { where(status: "Published") }
  scope :unpublished, -> { where(status: "Unublished") }


  ### Relations
  has_many :collection_items
  has_many :collections, through: :collection_items
  has_many :tags, through: :taggings
  has_many :taggings, dependent: :destroy
  has_many :asset_people, dependent: :destroy
  has_many :people, through: :asset_people
  has_many :favorites, dependent: :destroy
  has_many :asset_views, dependent: :destroy

  ### Sunspot search
  searchable do
    text :title, :default_boost => 6
    text :description
    time :date_from
    time :date_to
    string :archive_reference
    string :archive_original_code
    string :medium
    boolean :colour
    string :status
    string :table_name do Asset.table_name end
    string :people, :multiple => true do
      people.map(&:name)
    end
    string :people_nationality, :multiple => true do
      people.map { |people| people.nationality}
    end
    string :tags, :multiple => true do
      tags.map { |tag| tag.name}
    end

  end

  ### Validations
  validates :file, :presence => true, :unless => :video?

  # Think this is making the files without them not save, change once all files are imported.
  # validates :title, :description, :presence => true

  ### Callbacks

  ## TODO - Make sure only admins can create, delete and update resources
  before_save :extract_dimensions, :parse_video_urls
  before_create :set_unpublished
  
  serialize :dimensions


  ## Library
  has_url :url, :generated_from => :title

  ### Paperclip
  has_attached_file :file,
                    :styles => lambda {|a| a.instance.check_file_type},
                    :processors => lambda{ |attachment|
                       attachment.check_processor_type
                    },
                    :default_url => ":style/missing.png",
                    :path => "public/asset_images/:id/:id_:style_:filename",
                    :url => "/asset_images/:id/:id_:style_:filename",
                    # :default_url => "asset_placeholders/:style.png",
                    :convert_options => { :all => "-strip -colorspace sRGB" },
                    :whiny => true,
                    :storage => :filesystem

  validates_attachment_presence :file, :unless => :video?
  validates_attachment_content_type :file, :content_type => [/\Aimage\/.*\Z/, /\Avideo\/.*\Z/, /\Aaudio\/.*\Z/, /\Aapplication\/.*\Z/]

  ## Instance Methods
  def user_collections(user)
    return self.collections.where(:user_id => user.id)
  end

  def title
    self[:title] ||= "Untitled"
  end

  def image?
    return ImageContentTypes.include?(self.file_content_type)
  end

  

  def check_file_type
    if self.media_type == "image"
      { :large => "800x800>", :medium => "300x300>", :thumb => "100x100>" }
    elsif self.media_type == "video"
      {
          :medium => { :geometry => "640x480", :format => 'mp4' },
          :thumb => { :geometry => "300x300#", :format => 'jpg', :time => 10 }
      }
    elsif self.media_type == "audio"
      {

      }
    else
      {}
    end
  end

  def check_processor_type
    if self.media_type == "image"
      [:thumbnail]
    elsif self.media_type == "video"
      [:ffmpeg]
    else
      {}
    end
  end


  def media_type
    if self.file_content_type =~ %r(video)
      return "video"
    elsif self.file_content_type =~ %r(audio)
      return "audio"
    elsif self.file_content_type =~ %r(image)
      return "image"
    elsif self.file_content_type =~ %r(application)
      return "application"
    end
  end

  ### Class methods
  # def self.viewable
  #   unless current_user && current_user.admin?
  #     self.published
  #   end
  # end


  ## Private Methods
  private

  def parse_video_urls
    return unless self.video?
    if self.video_url.include?('youtube.com') & self.video_url.exclude?('embed')
      v_id = /(v=)(.+)$/.match(self.video_url)[2]
      self.video_url = "https://www.youtube.com/embed/#{v_id}"
      self.thumbnail_url = "https://img.youtube.com/vi/#{v_id}/sddefault.jpg"
    elsif self.video_url.include?('vimeo.com') & self.video_url.exclude?('player')
      v_id = /(.com\/)(.+)$/.match(video_url)[2]
      self.video_url = "https://player.vimeo.com/video/#{v_id}" 
      self.thumbnail_url = vimeo_thumb_url(v_id)
    end
  end

  def vimeo_thumb_url(v_id)
    url = "https://vimeo.com/api/oembed.json?url=https://vimeo.com/#{v_id}"
    result = Net::HTTP.get(URI.parse(url))
    return JSON.parse(result)['thumbnail_url']
  end

  # Retrieves dimensions for image assets
  # @note Do this after resize operations to account for auto-orientation.
  def extract_dimensions
    return unless image?
    tempfile = file.queued_for_write[:original]
    unless tempfile.nil?
      geometry = Paperclip::Geometry.from_file(tempfile)
      self.dimensions = [geometry.width.to_i, geometry.height.to_i]
    end
  end

  def set_unpublished
    self.status = "Unpublished"
  end

end

# == Schema Information
#
# Table name: assets
#
#  id                       :integer          not null, primary key
#  asset_type               :string(255)
#  title                    :string(255)
#  archive_original_code    :string(255)
#  archive_reference        :string(255)
#  acquisition_source       :string(255)
#  description              :text
#  medium                   :string(255)
#  original_scan_resolution :string(255)
#  bit_depth                :string(255)
#  date_from                :date
#  date_to                  :date
#  dimensions               :string(255)
#  deleted                  :boolean
#  file_file_name           :string(255)
#  file_content_type        :string(255)
#  file_file_size           :integer
#  file_updated_at          :datetime
#  created_at               :datetime
#  updated_at               :datetime
#  colour                   :boolean
#  status                   :string
#  cached_views             :integer
#  video                    :boolean
#  video_url                :string
#  thumbnail_url            :string
#
