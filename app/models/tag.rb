class Tag < ActiveRecord::Base
  
  ### Model def

  has_many :assets, through: :taggings
  has_many :taggings
  
  ### Validations
  validates :name, :presence => true, :uniqueness => true
  
  
  ## Library
  has_url :url, :generated_from => :name
  
end

# == Schema Information
#
# Table name: tags
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#
