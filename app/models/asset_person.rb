class AssetPerson < ActiveRecord::Base
  ## Relations  
  belongs_to :asset
  belongs_to :person
  
  
  ## Validations
  validates_uniqueness_of :asset_id, :scope => :person_id
  
  def name
  
    return @pid
    
  end
end

# == Schema Information
#
# Table name: asset_people
#
#  id         :integer          not null, primary key
#  asset_id   :integer
#  person_id  :integer
#  created_at :datetime
#  updated_at :datetime
#
