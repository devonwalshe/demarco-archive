class AssetView < ActiveRecord::Base
  paginates_per 24
  ### Model def

  belongs_to :user
  belongs_to :asset

  ### Validations

  ## Library

end

# == Schema Information
#
# Table name: asset_views
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  asset_id   :integer
#  context    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
