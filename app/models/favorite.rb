class Favorite < ActiveRecord::Base
  ## Associations
  paginates_per 24
  belongs_to :user
  belongs_to :asset

  ## Validations


  ## Callbacks


end

# == Schema Information
#
# Table name: favorites
#
#  id         :integer          not null, primary key
#  asset_id   :integer
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#
