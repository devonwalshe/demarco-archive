class Collection < ActiveRecord::Base
  ## Pagination
  paginates_per 24

  ## Relationships
  has_many :collection_items, :dependent => :destroy
  has_many :assets, through: :collection_items
  belongs_to :user

  ## Can belong to self
  has_many :children, class_name: "Collection", foreign_key: "parent_id"
  belongs_to :parent, class_name: "Collection"

  belongs_to :front_image, :class_name => 'Asset'

  ## Scopes
  scope :by_asset_count, -> { order('collection_items_count DESC') }
  scope :global,  -> { where(public: true) }
  scope :personal, -> { where(public: false) }
  scope :featured, -> { where(featured: true) }
  scope :archive_collections, -> { where(user_id: 0) }
  scope :user_collections, -> (user) {where(user_id: user.id)}

  ## Validations
  validates :title, :presence => true, :uniqueness => true

  ### Sunspot search

  searchable do
    text :title, :boost => 2
    text :description
    integer :collection_items_count
    boolean :public
    integer :user_id
  end

  ## Instance methods
  has_url :url, :generated_from => :title

  def ancestors
    return @ancestors ||= (parent ? (parent.ancestors + [self]) : [self])
  end
  
  def has_parent?
    
  end

  def has_parent?
    return self.parent_id != nil
  end

  ## Class methods
  def self.archive_collection
    return Collection.where(user_id: nil).first
  end

  def self.admin_collections(admin_user)
    if admin_user && admin_user.admin?
      return self.where(:user_id => ["0", admin_user.id])
    else
      return nil
    end
  end
  # scope :main_collections -> { where(:parent_id == nil) }
end

# == Schema Information
#
# Table name: collections
#
#  id                     :integer          not null, primary key
#  title                  :string(255)
#  user_id                :integer
#  public                 :boolean
#  front_image_id         :integer
#  description            :text
#  created_at             :datetime
#  updated_at             :datetime
#  collection_items_count :integer          default(0)
#  parent_id              :integer
#  featured               :boolean
#
# Indexes
#
#  index_collections_on_parent_id  (parent_id)
#
