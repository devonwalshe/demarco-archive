class CollectionItem < ActiveRecord::Base
  ## Relations  
  belongs_to :asset
  belongs_to :collection, :counter_cache => true
  
  ## Validations
  validates_uniqueness_of :asset_id, :scope => :collection_id
  
  
  
end

# == Schema Information
#
# Table name: collection_items
#
#  id            :integer          not null, primary key
#  collection_id :integer
#  asset_id      :integer
#  created_at    :datetime
#  updated_at    :datetime
#
