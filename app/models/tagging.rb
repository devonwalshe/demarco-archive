class Tagging < ActiveRecord::Base
  ### Relations
  belongs_to :asset
  belongs_to :tag
  
  ## Validations
  validates_uniqueness_of :asset_id, :scope => :tag_id
end

# == Schema Information
#
# Table name: taggings
#
#  id         :integer          not null, primary key
#  asset_id   :integer
#  tag_id     :integer
#  created_at :datetime
#  updated_at :datetime
#
