class Person < ActiveRecord::Base

  ## Relationships
  has_many :asset_people, :dependent => :destroy
  has_many :assets, through: :asset_people
  
  belongs_to :front_image, :class_name => 'Asset'
  
  ## Validations
  validates :name, :presence => true, :uniqueness => true
  
  ### Instance methods
  has_url :url, :generated_from => :name
  
  ### Sunspot search
  
  searchable do
    text :name
    string :nationality
    string :role
    string :table_name do Person.table_name end
    
  end
  
end

# == Schema Information
#
# Table name: people
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  nationality    :string(255)
#  dob            :datetime
#  role           :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  bio            :text
#  front_image_id :integer
#
