# == Schema Information
#
# Table name: collection_items
#
#  id            :integer          not null, primary key
#  collection_id :integer
#  asset_id      :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class CollectionItemsController < ApplicationController
  
  def create
    @collection_item = CollectionItem.new(collection_item_params)

    respond_to do |format|
      if @collection_item.save
        format.html { }
        format.json { render json: @collection_item, status: :created, location: @collection_item_url }
      else
        format.html { }
        format.json { render json: {collection_item: @collection_item, errors: @collection_item.errors }, status: :unprocessable_entity}
      end
    end
    
  end

  def delete
    @collection_item = CollectionItem.where(["asset_id = ? and collection_id = ?", collection_item_params[:asset_id], collection_item_params[:collection_id]]).first

    @collection_item.destroy
    respond_to do |format|
      format.html { }
      format.json { head :no_content }
    end
    
  end
  
  private
  
  def collection_item_params
    params.require(:collection_item).permit(:asset_id, :collection_id)
  end
  
end
