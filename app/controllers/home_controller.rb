class HomeController < ApplicationController

  def index
    @static_text = StaticText.first
    if !current_user.nil?
      user_asset_ids = current_user.asset_views.collect {|av| av.asset_id}.uniq
      user_assets = Asset.where(id: user_asset_ids)
      @recently_viewed = user_assets.includes(:asset_views).order('asset_views.updated_at DESC').limit(16)#.page params[:page]
      @popular = Asset.order(cached_views: :desc).limit(12)

    else
      @recently_viewed = Asset.joins(:asset_views).group('assets.id').order('max(asset_views.created_at) DESC').limit(12)
      @popular = Asset.published.order(cached_views: :desc).limit(12)
    end
    
    @featured_collections = Collection.featured.order(:updated_at)
    render 'home/index'
  end

  def about
    @static_text = StaticText.first
  end

  def send_contact
    contact_message = {contact_name: params[:name], contact_email: params[:email], contact_message: params[:message]}
    print(ContactMailer.send_contact_message(contact_message))
    ContactMailer.send_contact_message(contact_message).deliver_now
    flash[:notice] = "Thank you for sending your message, we'll get back to you soon"
    redirect_to "/about"
  end

  def errors
    status_code = params[:code] || 500
    render status_code.to_s, status: status_code, template: "home/#{status_code}"
  end

end
