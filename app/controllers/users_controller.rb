# == Schema Information
#
# Table name: users
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  email             :string(255)
#  remember_digest   :string(255)
#  password_digest   :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  admin             :boolean
#  activation_digest :string
#  activated         :boolean          default(FALSE)
#  activated_at      :datetime
#  reset_digest      :string
#  reset_sent_at     :datetime
#
# Indexes
#
#  index_users_on_email            (email) UNIQUE
#  index_users_on_remember_digest  (remember_digest)
#

class UsersController < ApplicationController

  before_action :check_permissions, only: [:edit, :destroy, :update]

  def index
    if current_user && current_user.admin?
      @users = User.all.order('LOWER(name)').page params[:page]
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
        if @user.save
          @user.send_activation_email
          flash[:success] = "Please check your email to activate your account"
          redirect_to root_url
        else
          flash[:error] = "Something went wrong with the form"
          render "new"
        end
  end

  def edit

  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'Person was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def history
    if logged_in?
      @user = current_user
      @history = Asset.joins(:asset_views).group('assets.id').where("asset_views.user_id = #{@user.id}").order('max(asset_views.created_at) DESC').page params[:page]
    else
      flash[:notice] = "You must be logged in to access this page"
      redirect_to "/signin"
    end
  end

  def favorites
    if logged_in?
      @user = current_user
      @favorites = @user.favorites.page params[:page]
    else
      flash[:notice] = "You must be logged in to access this page"
      redirect_to "/signin"
    end
  end

  def dashboard
    id = get_id(params[:url])
    if logged_in? && id == current_user.id.to_s

      @user = current_user
      @collections = @user.collections
      @favorites = @user.favorites
      @stats = {
                :total_views => AssetView.where(:user_id => @user.id).count,
                :favorites => @favorites.count,
                :collections => @collections.count,
                :joined => @user.created_at.strftime("%A, %b %d, %Y")
      }
      @history = Asset.joins(:asset_views).group('assets.id').where("asset_views.user_id = #{@user.id}").order('max(asset_views.created_at) DESC')
    else
      flash[:notice] = "You must be logged in as the owner of this page"
      redirect_to "/signin"
    end

  end

  def toggle_admin
    if logged_in? && current_user.admin?
      @user = User.find(params[:url])
      # @user.toggle_admin_status

      respond_to do |format|
        if @user.update_attribute(:admin, params[:user][:admin] )
          format.html {}
          format.js {render json: @user.updated_at}
        else
          format.html {}
          format.js {render json: {errors: @user.errors }, status: :unprocessable_entity}
          flash[:error] = "Something went wrong"
        end
      end

    else
      flash[:error] = "You can't do this"
      redirect_to :root
    end
  end

  private

  def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :admin)
  end

  def check_permissions
    unless current_user
      redirect_to root_path, notice: "User account needed."
    end

    @action = params[:action]

    case @action
      when "edit", "update", "destroy"
        @user = User.find(params[:id])

        unless (current_user && current_user.admin?) || (current_user && current_user == @user)
          redirect_to root_path, notice: "Admin needed."
        end
    end
  end

end
