# == Schema Information
#
# Table name: asset_people
#
#  id         :integer          not null, primary key
#  asset_id   :integer
#  person_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class AssetPeopleController < ApplicationController

  def create
    @asset_person = AssetPerson.new(asset_person_params)

    respond_to do |format|
      if @asset_person.save
        format.html { }
        format.json {  render json: @asset_person,status: :created, location: @asset_person_url }
      else
        format.html {  }
        format.json { render json: {person: @asset_person, errors: @asset_person.errors }, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @asset_person = AssetPerson.where(["asset_id = ? and person_id = ?", asset_person_params[:asset_id], asset_person_params[:person_id]]).first

    @asset_person.destroy
    respond_to do |format|
      format.html { }
      format.json { head :no_content }
    end
  end

  private



    # Never trust parameters from the scary internet, only allow the white list through.
    def asset_person_params
      params.require(:asset_person).permit(:asset_id, :person_id)
    end

end
