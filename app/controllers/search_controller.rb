class SearchController < ApplicationController

  def search

    ## Instance variables
    @people = Person.all.order(name: :asc)

    @tags = Tag.all.map{|t| t.name}.sort
    @nationalities = Person.all.select{|p| p.nationality != nil}.map{|p| p.nationality}.uniq!.sort


    if current_user
      if current_user.admin?
        @mediums = Asset.all.select{|a| a.medium != nil}.map{|a| a.medium}.uniq!.sort
        @catalogue_numbers = Asset.all.select{|a| a.archive_reference != nil}.map{|a| a.archive_reference}.uniq!.compact.sort
      else
        @mediums = Asset.published.select{|a| a.medium != nil}.map{|a| a.medium}.uniq!.sort
        @catalogue_numbers = Asset.published.select{|a| a.archive_reference != nil}.map{|a| a.archive_reference}.uniq!.compact.sort
      end
    else
      @mediums = Asset.published.select{|a| a.medium != nil}.map{|a| a.medium}.uniq!.sort
      @catalogue_numbers = Asset.published.select{|a| a.archive_reference != nil}.map{|a| a.archive_reference}.uniq!.compact.sort
    end

    if params[:search_type] == "assets"
      
      ### Asset search
      @search = Sunspot.search Asset do |query|
        
        if current_user
          if current_user.admin?
            if params[:status] && params[:status] == "unpublished"
              query.with(:status, "Unpublished")
            else
              query.with(:status).any_of(["Published", "Unpublished"])
            end
          else
            query.with(:status, "Published")
          end
        else
          query.with(:status, "Published")
        end

        # General Keyword query
        if params[:q]
          query.fulltext params[:q] do
            minimum_match 2
          end
        end


        ## Date queries
        if params[:after] && params[:after] != ""
          date = Date.parse(params[:after])
          query.with(:date_from).greater_than(date)
        end

        if params[:before] && params[:before] != ""
          date = Date.parse(params[:before])
          query.with(:date_from).less_than(date)
        end

        if params[:between]
          start_date = Date.parse(params[:between].split("-")[0])
          end_date = Date.parse(params[:between].split("-")[1])
          query.with(:date_from).between(start_date..end_date)
        end

        if params[:on]
          date = Date.parse(params[:on])
          query.with(:date_from).between(date..date)
        end


        ## Person
        if params[:people]
          people = params[:people]
          if params[:people_combo]=="all"
            query.with(:people).all_of(people)
          else
            query.with(:people).any_of(people)
          end

        end

        if params[:nationality]
          nationality = params[:nationality]
          if params[:nationality_combo]=="all"
            query.with(:people_nationality).all_of(nationality)
          else
            query.with(:people_nationality).any_of(nationality)
          end
        end

     ## Metadata Queries TODO - fix these so that they handle single entries

        # Medium - uses an example of passing in an optional all, with comma seperated values
        if params[:medium]
          # medium_array = params[:medium]
          query.with(:medium, params[:medium])
        end

        ## Archive codes
        if params[:archive_original_code]
          query.with(:archive_original_code, params[:archive_original_code])
        end

        if params[:archive_reference]
          query.with(:archive_reference, params[:archive_reference])
        end

        if params[:tags]
          tags_array = params[:tags]
          if params[:tags_combo] == "all"
            query.with(:tags).all_of(tags_array)
          else
            query.with(:tags).any_of(tags_array)
          end
        end


        if params[:colour]
          if params[:colour] == "true"
            query.with(:colour, true)
          elsif params[:colour] == "false"
            query.with(:colour).any_of([true, false])
          end
        end

        ## Untitled
        # if params[:title]
        #   query.with(:title, params[:title])
        # end
        query.paginate :page => params[:page], :per_page => 20
      end

    ## Search for collections
    elsif params[:search_type] == 'collections'


      @search = Sunspot.search Collection do |query|

        ### Defaults to showing public collections only
        ### Only the 'mine' param will show private collections, and only those belonging to the signed in user.

        ## Logged in validation for mine only scoping, with private collections
        if params[:mine] == 'true' && logged_in?

            query.with(:public).any_of([true, false])
            query.with(:user_id).equal_to(current_user.id)

            if params[:asset_count]
              query.with(:collection_items_count).greater_than(params[:asset_count].to_i-1)
            end

        else

          ## Default param scoping available to everybody
          ## Default search
          query.with(:public, true)
          query.keywords params[:q]

          ## Items count

          if params[:asset_count]
            query.with(:collection_items_count).greater_than(params[:asset_count].to_i-1)
          end

          ## Just public user collections
          if params[:user_collections]

            if params[:user_collections] == 'true'
              query.with(:user_id).greater_than(1)
            end

            if params[:mine]
              query.with(:public).any_of([true, false])
            else
              query.with(:public, true)
            end
          end

        end
        query.paginate :page => params[:page], :per_page => 22
      end


    ## Search for people
    elsif params[:search_type] == 'people'

      @search = Sunspot.search Person do |query|
        query.keywords params[:q]

        if params[:nationality]
          query.with(:nationality, params[:nationality])
        end

        if params[:name]
          query.with(:name).any_of(params[:name])
        end

      end

    ## Search everything
    else
      ### General search
      @search = Sunspot.search Asset, Collection, Person do |query|

        if !current_user || (current_user && !current_user.admin?)
          query.without(:status, "Unpublished")
        end

        query.fulltext params[:q]


        query.without(:public, false)



        if params[:name]
          query.with(:name).equal_to(params[:name])
        end

        query.paginate :page => params[:page], :per_page => 20
      end

    end


    respond_to do |format|
      format.html { render "search"}
      format.js {}
    end
  end


end
