# == Schema Information
#
# Table name: taggings
#
#  id         :integer          not null, primary key
#  asset_id   :integer
#  tag_id     :integer
#  created_at :datetime
#  updated_at :datetime
#

class TaggingsController < ApplicationController

  def show
  end
  def create
    @tagging = Tagging.new(tagging_params)

    respond_to do |format|
      if @tagging.save
        format.html { }
        format.json { render json: @tagging, status: :created, location: @tagging_url }
      else
        format.html { }
        format.json { render json: {tagging: @tagging, errors: @tagging.errors }, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @tagging = Tagging.where(["asset_id = ? and tag_id = ?", tagging_params[:asset_id], tagging_params[:tag_id]]).first

    @tagging.destroy
    respond_to do |format|
      format.html { }
      format.json { head :no_content }
    end
  end

  private
    def tagging_params
      params.require(:tagging).permit(:asset_id, :tag_id)
    end
end
