# == Schema Information
#
# Table name: people
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  nationality    :string(255)
#  dob            :datetime
#  role           :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  bio            :text
#  front_image_id :integer
#

class PeopleController < ApplicationController
  before_action :set_person, only: [:show, :edit, :update, :destroy]

  # GET /people
  # GET /people.json
  def index
    # @people = Person.all
    # @person = Person.new
    redirect_to "/search?search_type=people"
  end

  # GET /people/1
  # GET /people/1.json
  def show
    url_rewrite(Person, params, flash)
    if logged_in? && current_user.admin?
      @assets = @person.assets.page(params[:page]).order(params[:order])
    else
      @assets = @person.assets.published.page(params[:page]).order(params[:order])
    end
  end

  # GET /people/new
  def new
    if current_user && current_user.admin?
      @person = Person.new
    else
      flash[:notice] = "You do not have access to this page"
      redirect_to "/"
    end
  end

  # GET /people/1/edit
  def edit
    if current_user && current_user.admin?
      url_rewrite(Person, params, flash)
    else
      flash[:notice] = "You do not have access to this page"
      redirect_to "/"
    end
     
  end

  # POST /people
  # POST /people.json
  def create
    @person = Person.new(person_params)
    respond_to do |format|
      if @person.save
        format.html { redirect_to @person, notice: 'Person was successfully created.' }
        format.json { render action: 'show', status: :created, location: @person, json: @person }
      else
        format.html { render action: 'new' }
        format.json { render json: {person: @person, errors: @person.errors }, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    puts "***** #{params} *****"
    # params[:person][:dob] = params["dob"]
    respond_to do |format|
      if @person.update(person_params)
        format.html { redirect_to @person, notice: 'Person was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    puts "here"
    @person = Person.find(params[:url])
    @person.destroy
    respond_to do |format|
      format.html { redirect_to "/", notice: "Person was successfully deleted" }
      format.json { head :no_content }
    end
  end

  private
  
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:url].split("-").first)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def person_params
      params.require(:person).permit(:name, :nationality, :dob, :role, :front_image_id, asset_people_attributes:[:id, :asset_id, :person_id, :_destroy])
    end
end
