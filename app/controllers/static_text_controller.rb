class StaticTextController < ApplicationController
  
  def edit
    @static_text = StaticText.first
  end
  
  def update
    @static_text = StaticText.first
    # puts(params[:static_text])
    # redirect_to dashboard_path(:url => current_user.url)
    puts params['static_text']
    if @static_text.update_attributes(static_text_params)
      redirect_to dashboard_path(:url => current_user.url)
    else
      flash[:error] = @static_text.errors
      render action: 'edit'
    end
    
  end
  
  private

  def static_text_params
     params.require(:static_text).permit(:front_header, :front_introduction, :front_quote, :about_title, :about_body, :about_copyright)
  end
  
end
