# == Schema Information
#
# Table name: favorites
#
#  id         :integer          not null, primary key
#  asset_id   :integer
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class FavoritesController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: [:create, :destroy]



  def create
    @fav = Favorite.new
    @fav.user_id = params[:user_id]
    @fav.asset_id = params[:asset_id]
    @asset = Asset.find(params[:asset_id])
    # @fav.save
    respond_to do |format|
      if @fav.save
        format.html {redirect_to :back}
        format.js {}
      else
        redirect_to :back
      end

    end
  end


  def destroy

    @favorite = Favorite.find(params[:id])
    @asset = Asset.find(@favorite.asset.id)
    # @favorite.destroy
    respond_to do |format|
      if @favorite.destroy
        format.html {redirect_to :back}
        format.js {}
      else
        redirect_to :back
      end
    end
  end


end
