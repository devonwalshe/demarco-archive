# == Schema Information
#
# Table name: tags
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class TagsController < ApplicationController

  def index
    @tag = Tag.all
  end

  def create
    @tag = Tag.new(tag_params)

    print params[:name]
    print "boop"
    # @fav.save
    respond_to do |format|
      if @tag.save
        format.html {}
        format.js { render json: @tag, status: :created, location: @tag_url }
      else
        format.json { render json: {tag: @tag, errors: @tag.errors }, status: :unprocessable_entity}
      end

    end
  end

  def show

  end

  def destroy

    @tag = Tag.find(params[:id])
    @asset = Asset.find(@tag.asset.id)
    # @favorite.destroy
    respond_to do |format|
      if @favorite.destroy
        format.html {redirect_to :back}
        format.js {}
      else
        redirect_to :back
      end
    end
  end

  private
    def tag_params
      params.require(:tag).permit(:name)
    end
end
