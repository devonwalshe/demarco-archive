# == Schema Information
#
# Table name: assets
#
#  id                       :integer          not null, primary key
#  asset_type               :string(255)
#  title                    :string(255)
#  archive_original_code    :string(255)
#  archive_reference        :string(255)
#  acquisition_source       :string(255)
#  description              :text
#  medium                   :string(255)
#  original_scan_resolution :string(255)
#  bit_depth                :string(255)
#  date_from                :date
#  date_to                  :date
#  dimensions               :string(255)
#  deleted                  :boolean
#  file_file_name           :string(255)
#  file_content_type        :string(255)
#  file_file_size           :integer
#  file_updated_at          :datetime
#  created_at               :datetime
#  updated_at               :datetime
#  colour                   :boolean
#  status                   :string
#  cached_views             :integer
#  video                    :boolean
#  video_url                :string
#  thumbnail_url            :string
#

class AssetsController < ApplicationController
  layout "empty", :only => :lightbox

  before_action :must_be_admin, only: [:new, :edit, :delete, :create, :update, :form_update]

  def index
  # @assets = Asset.all
   @assets = Asset.order(:id).page params[:page]
  end

  def show
    url_rewrite(Asset, params, flash)

    if @asset.status == "Unpublished" && (!logged_in? || (current_user && !current_user.admin?))
      raise ActionController::RoutingError.new('Not Found')
    end

    @collections = @asset.collections.global.archive_collections

    # Any user with an account can add images to their collections
    if logged_in? && current_user.admin?
      @user_collections = current_user.collections + Collection.where(user_id: 0) 
    elsif logged_in?
      @user_collections = current_user.collections
    end

    @tags = @asset.tags
    view_counter(@asset, action_name)
  end

  def edit
    @collections = Collection.all
    url_rewrite(Asset, params, flash)
    @asset_people = @asset.people
    @asset_people_json = @asset_people.collect {|p| p.id}.reject(&:blank?).join(",")
    @people = Person.all
    @people_json = @people.collect {|p| {"id": p.id, "text":p.name} }.to_json
    @id = get_id(params[:url])
    @tags = Tag.all
    @tags_json = @tags.collect {|p| {"id": p.id, "text":p.name} }.to_json
    @assettags = @asset.tags
    @assettags_json = @assettags.collect {|p| p.id}.reject(&:blank?).join(",")
  end

  def update
    @people = Person.all
    id = get_id(params[:url])
    @asset = Asset.find(id)

    if @asset.update(asset_params)
      if params[:collections].present?
        @old_items = @asset.collection_items
        @old_items.each do |old_item|
          CollectionItem.destroy(old_item.id)
        end

        params[:collections].each do |collection|
          @collection_item = CollectionItem.new(:asset_id => id, :collection_id => collection )
          @collection_item.save
        end
      end

      params[:url] = @asset.url

      @collections = @asset.collections.global
      @tags = Asset.find(id).tags
      flash[:notice] ='Asset was successfully updated.'
      # render action: 'show'
      redirect_to @asset
    else
      flash[:error] = @asset.errors

      render action: 'edit'
    end
  end

  def delete
    @id = get_id(params[:url])
    @asset = Asset.find(@id)
    @asset.destroy
    flash[:notice] ='Asset was successfully deleted'
    redirect_to root_url
  end

  def new
    @asset = Asset.new
  end


  def create
    @asset = Asset.new(asset_params)
    puts "### #{@asset.video} ####}"
    if @asset.save
      flash[:notice] = 'Asset was successfully created. Add more detailed information here'
      redirect_to "/assets/#{@asset.url}/edit"
    else
      flash[:error] = "Could not save asset"
      render action: 'new'
    end

  end

  def get_people
    @people = Person.all
    render js: "get_people.js"
  end

  def form_update
    respond_to do |format|
      format.html
      format.json  { render json: @test, status: :created, location: @test }
    end
  end

  def lightbox
    url_rewrite(Asset, params, flash)
    @collections = @asset.collections.global
    @tags = @asset.tags
    view_counter(@asset, action_name)
    render "lightbox"
  end

  private

  def asset_params
     params.require(:asset).permit(:title, :file, :description, :archive_original_code, :medium, :archive_reference, :id, :colour, :status, :date_from, :date_to, :video, :video_url)
  end

  def view_counter(asset, action_name)
    vc = AssetView.new
    if logged_in?
      vc.user_id = current_user.id
    else
      vc.user_id = 0
    end
    vc.asset_id = asset.id
    vc.context = "#{controller_name}\##{action_name}"
    asset.cached_views = asset.asset_views.count
    asset.save
    vc.save
  end

  private
    def must_be_admin
      unless current_user && current_user.admin?
        redirect_to root_path, notice: "Admin Needed."
      end
    end


end
