# == Schema Information
#
# Table name: collections
#
#  id                     :integer          not null, primary key
#  title                  :string(255)
#  user_id                :integer
#  public                 :boolean
#  front_image_id         :integer
#  description            :text
#  created_at             :datetime
#  updated_at             :datetime
#  collection_items_count :integer          default(0)
#  parent_id              :integer
#  featured               :boolean
#
# Indexes
#
#  index_collections_on_parent_id  (parent_id)
#

class CollectionsController < ApplicationController
  # before_action :set_collection, only: [:show, :edit, :update, :destroy]
  # before_action :must_be_user, only: [:new, :edit, :destroy, :create, :update]
  before_action :check_permissions, only: [:new, :edit, :destroy, :create, :update]


  # GET /collections
  # GET /collections.json
  def index
    if logged_in?
      @user = current_user
      @personal_collections = @user.collections
    end
    if logged_in? && current_user.admin?
      @user_collections = Collection.where.not(user_id: 0)
    else
      @user_collections = Collection.global.where.not(user_id: 0)
    end

    @archive_collection = Collection.archive_collection
    @archive_collections = @archive_collection.children.order(title: :asc)


  end

  # GET /collections/1
  # GET /collections/1.json
  def show
    if logged_in?
      @user = current_user
    end
    url_rewrite(Collection, params, flash)
    @archive_collection = Collection.archive_collection
    if logged_in? && (@collection.user_id == current_user.id || current_user.admin?)
      @children = @collection.children.order(title: :asc).page params[:page]
      @assets = @collection.assets.page params[:page]
    else
      @children = @collection.children.global.order(title: :asc).page params[:page]
      @assets = @collection.assets.published.page params[:page]
    end
    
    # @assets = @collection.assets.page params[:page]
    @ancestors = @collection.ancestors
  end

  def public_collections

  end


  # GET /collections/new
  def new
    if params[:sub] == "true"
      @parent = Collection.find(params[:parent_id])
    else
      @parent = nil
    end

    @collection = Collection.new
    @archive_collection = Collection.archive_collection
  end

  # GET /collections/1/edit
  def edit
    url_rewrite(Collection, params, flash)

    if @collection.has_parent?
      @parent = Collection.find(@collection.parent_id)
    else
      @parent = nil
    end

    @archive_collection = Collection.archive_collection

    @children = @collection.children.global.page params[:page]
    @assets = @collection.assets.page params[:page]
  end

  # POST /collections
  # POST /collections.json
  def create
    @collection = Collection.new(collection_params)

    if @collection.save
      flash[:notice] = "Collection was successfully created."
      redirect_to @collection
    else
      flash[:error] = "Collection could not be saved"
      render action: 'new'
    end
  end

  # PATCH/PUT /collections/1
  # PATCH/PUT /collections/1.json
  def update
    id = get_id(params[:url])
    
    @collection = Collection.find(id)

    if @collection.update(collection_params)
      redirect_to @collection
    else
      render action: 'edit'
    end

  end

  # DELETE /collections/1
  # DELETE /collections/1.json
  def destroy
    @collection = Collection.find(params[:url])
    @collection.destroy
    respond_to do |format|
      format.html { redirect_to collections_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_collection
      @collection = Collection.find(params[:url].split('-').first )
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def collection_params
      params.require(:collection).permit(:title, :user_id, :global, :front_image_id, :description, :public, :featured, :parent_id)
    end

    def must_be_user
      unless current_user
        redirect_to root_path, notice: "Admin Needed."
      end
    end

    def check_permissions
      unless current_user
        redirect_to root_path, notice: "User account needed."
      end

      @action = params[:action]

      case @action
        when "new","create"
          @parent = params[:sub] == "true" ? Collection.find(params[:parent_id]) : nil

          @archive_collection = Collection.archive_collection

          unless (current_user && current_user.admin?) || (@parent != nil && current_user == @parent.user) || (@parent == nil && @parent != @archive_collection )
            redirect_to root_path, notice: "Admin needed."
          end

        when "edit", "update", "destroy"
          @collection = Collection.find(params[:url])

          if @collection == Collection.archive_collection
            redirect_to root_path, notice: "Cannot edit root collection."
          end

          unless (current_user && current_user.admin?) || (current_user && current_user == @collection.user)
            redirect_to root_path, notice: "Admin needed."
          end
      end
    end
end
