var do_on_load = function() {
  $("#edit-asset-collection").select2();
  $("#add-asset-collections").select2();
  var assetId = $("#asset_people").data('asset-id');
  var people = $('#asset_people').data('asset-people');
  var tags = $('#asset_tags').data('asset-tags');
  $("#asset_tags").select2({
    createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.toUpperCase().indexOf(term.toUpperCase())>=0; }).length===0) {return {id:term, text:term};} },
    multiple: true,
    data: tags
  });
  
  $("#asset_tags").on("change", function(e){
    console.log(e.added);
    if (e.added && typeof e.added.id === 'string') {
      addTag();
    } else if (e.added && typeof e.added.id === 'number') {
      addTagging(e.added.id);
    } else if (e.removed) {
      removeTagging();
    }
    // Add tag on view image
    function addTagging(tagId) {
      $.ajax({
        type: "POST",
        url: "/taggings",
        dataType: "json",
        data: {tagging: {tag_id: tagId, asset_id: assetId }},
        success: function(data){
          console.log(data);
        },
        error: function(data){
          console.log(data.responseJSON);
        }
      });
    }
    
    function addTag() {
      $.ajax({
        type: "POST",
        url: "/tags",
        dataType: "json",
        data: {tag: {name: e.added.text}},
        success: function(data){
          var tagId = data.id
          addTagging(tagId);
        },
        error: function(data){
          console.log(data.responseJSON);
        }
      });
      // on success addAssetperson()
    }
    function removeTagging() {
      $.ajax({
        type: "DELETE",
        url: "/taggings",
        dataType: "json",
        data: {tagging: {tag_id: e.removed.id, asset_id: assetId }},
        success: function(data){
          console.log(data);
        },
        error: function(data){
          console.log(data.responseJSON);
        }
      });
    }
  });

  $("#asset_people").select2({
    createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.toUpperCase().indexOf(term.toUpperCase())>=0; }).length===0) {return {id:term, text:term};} },
    multiple: true,
    data: people
  });

  $("#asset_people").on("change", function(e){
    if (e.added && typeof e.added.id === 'string') {
      addPerson();
    } else if (e.added && typeof e.added.id === 'number') {
      addAssetPerson(e.added.id);
    } else if (e.removed) {
      removeAssetPerson();
    }

    function addAssetPerson(personId) {
      $.ajax({
        type: "POST",
        url: "/asset_people",
        dataType: "json",
        data: {asset_person: {person_id: personId, asset_id: assetId }},
        success: function(data){
          console.log(data);
        },
        error: function(data){
          console.log(data.responseJSON);
        }
      });
    }

    function addPerson() {
      $.ajax({
        type: "POST",
        url: "/people",
        dataType: "json",
        data: {person: {name: e.added.text}},
        success: function(data){
          var personId = data.id
          addAssetPerson(personId);
        },
        error: function(data){
          console.log(data.responseJSON);
        }
      });
    }

    function removeAssetPerson() {
      $.ajax({
        type: "DELETE",
        url: "/asset_people",
        dataType: "json",
        data: {asset_person: {person_id: e.removed.id, asset_id: assetId }},
        success: function(data){
          console.log(data);
        },
        error: function(data){
          console.log(data.responseJSON);
        }
      });
    }
  });
  
  $("#add-asset-collections").on("change", function(e){
    
    assetId = $("#add-asset-collections").data('asset-id');
    console.log(e.added);
    if (e.added) {
      addCollectionItem(e.added.id);
    } else if (e.removed) {
      removeCollectionItem();
    }

    function addCollectionItem(collectionId) {
      $.ajax({
        type: "POST",
        url: "/collection_items",
        dataType: "json",
        data: {collection_item: {collection_id: collectionId, asset_id: assetId }},
        success: function(data){
          console.log(data);
        },
        error: function(data){
          console.log(data.responseJSON);
        }
      });
    }

    function removeCollectionItem() {
      $.ajax({
        type: "DELETE",
        url: "/collection_items",
        dataType: "json",
        data: {collection_item: {collection_id: e.removed.id, asset_id: assetId }},
        success: function(data){
          console.log(data);
        },
        error: function(data){
          console.log(data.responseJSON);
        }
      });
    }
  });
  
}

$(document).ready(do_on_load);
