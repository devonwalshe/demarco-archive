var do_on_load = function() {
  $("[data-admin-form]").on("change", function(e){
    $(this).submit();
  });

  $('.toggle-button').click(function(){
    var userId = $(this).data('toggle');
    $('[data-admin-form="'+userId+'"]').trigger("change");
  });
}

$(".users.index").ready(do_on_load);
