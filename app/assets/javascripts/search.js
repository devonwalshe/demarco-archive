var do_on_load = function() {
  $("#filter-people").select2();
  $("#filter-mediums").select2();
  $("#filter-tags").select2();
  $("#filter-nationality").select2();
  $("#filter-catalogue_numbers").select2();
  $("#filter-people").select2().on("change", function(e){updateResults();});
  $("#filter-mediums").select2().on("change", function(e){updateResults();});
  $("#filter-tags").select2().on("change", function(e){updateResults();});
  $("#filter-nationality").select2().on("change", function(e){updateResults();});
  $("#filter-catalogue_numbers").select2().on("change", function(e){updateResults();});
  $(".filter-panel input[type='radio']").on("change", function(e){updateResults();});
  $('.toggle-button').on("click", function(e){updateResults();});

  // var panelShowing = false;
  // $('.advanced-filter-btn').click(function(e){
  //   e.preventDefault();
  //   if (!panelShowing){
  //     $('.filter-panel').velocity('slideDown', {duration: 100});
  //     panelShowing = true;
  //   }
  //   else {
  //     $('.filter-panel').velocity('slideUp', {duration: 100});
  //     panelShowing = false;
  //   }
  // });
  //
  $('#keyword-form').on("ajax:complete", function(){
    updateUrl('#keyword-form');
  })
}

$('.search.search').ready(do_on_load);

function updateResults(){
  // Check if query bar is filled and input that as our :q
  if($('#keyword-query').val().length > 0){
    $('#filter-query').val($('#keyword-query').val())
  } else {
    $('#filter-query').val("")
  }
  // Submit form
  $("#filterForm").submit();
  // Update Url
  updateUrl('#filterForm');
}

// Keyword update URL
var updateUrl = function(element){
  // Add serialized url to bar
  window.history.pushState("","", "/search?" +  $(element).serialize());
}
