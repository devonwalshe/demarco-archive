document.documentElement.className =
    document.documentElement.className.replace(/\bjs-error\b/,'');

window.onerror = function (e) {
  var root = document.documentElement;
  root.className += " js-error";
};

//This may help autosize the textarea, still just a test
function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (25+o.scrollHeight)+"px";
  }

function galleryInit() {
  $('.gallery-popup').magnificPopup({

    type: 'ajax',
    gallery: {
        enabled: true
    },
    callbacks: {
      parseAjax: function(mfpResponse) {
        mfpResponse.data = $(mfpResponse.data).filter('.lightbox-container');
      },
      ajaxContentAdded: function() {
        $('.mejs-player').mediaelementplayer();
      }
    }

  });
}

function formToggle() {
  formToggleDo($(".filter-toggle"));
  if($('.asset-colour-toggle').parent().parent().find('.filter-radio-all').prop('checked')){

    $('.asset-colour-toggle').addClass('toggleOn');
    // $('.asset-colour-toggle').find('.toggle-knob-holder').css({'left':'-6px'});
  }
  $('.toggle-button').click(function(){
    if ($(this).hasClass('toggleOn')) {
      $(this).removeClass('toggleOn');
      $(this).parent().parent().find('.filter-radio-all').prop("checked", false);
      $(this).parent().parent().find('.filter-radio-any').prop("checked", "checked");
    }
    else {
      $(this).addClass('toggleOn');
      $(this).parent().parent().find('.filter-radio-all').prop("checked", "checked");
      $(this).parent().parent().find('.filter-radio-any').prop("checked", false);
    }

  });
}

function videoToggle(){
  $('#video-url').hide()
  $('#video .toggle-button').click(function(){
    if ($(this).hasClass('toggleOn')) {
      $('#video-url').show()
      $('#file').hide()
    }
    else {
      $('#video-url').hide()
      $('#file').show()
    }
  })
  // documentElement.getElementById('video-url').hide()
}

function formToggleDo(item){
  $(item).find(".filter-radio").hide();
  $(item).find(".toggle-wrap").show();
}

$(document).ready(function(){
  galleryInit();
  formToggle();
  videoToggle();
  $(".gallery-view").infinitescroll({
    navSelector: "nav.pagination",
    nextSelector: "nav.pagination a[rel=next]",
    itemSelector: ".gallery"
    },

    function(arrayOfNewElems){
      galleryInit();
    }
  );
  $('.collection__list-parent.hasChildren').on('click', function(e){
    e.preventDefault();
    collectionListToggle($(this));
  })
});

function collectionListToggle(item) {
  var parent = $(item).parent();
  if (parent.hasClass('isOpen')) {
    collectionListClose($(parent));
  } else {
    if ($('.isOpen')) {
      $('.isOpen').find('.collection__list-children').removeAttr('style');
      $('.isOpen').removeClass('isOpen');
    }
    collectionListOpen($(parent));
  }
}

function collectionListClose(item) {
  console.log('Close List: '+item);
  item.find('.collection__list-children').velocity("slideUp");
  item.removeClass('isOpen');
}

function collectionListOpen(item) {
  console.log('Open List: '+item);
  item.find('.collection__list-children').velocity("slideDown");
  item.addClass('isOpen');
  item.velocity("scroll", {offset:-107});
}




// Overlays
var do_on_load = function() {

  // Modernizr
  if (Modernizr.input.placeholder) {
    $(".label-hide").hide();
  }

  //Probably not necessary. Browser support of contenteditable is comprehensive
  if (!Modernizr.contenteditable){
    //Description textarea replace
    d = document.getElementById("asset_description");
    dname = d.name;
    console.log(dname);
    did = d.id;
    eval = document.getElementById("editDescription").innerHTML;
    d.outerHTML = '<textarea id="'+did+'" class="field-description" name='+dname+'>'+eval+'</textarea>';
    document.getElementById("editDescription").outerHTML = "";


  }

  // Infinite Scroll

}

$(document).ready(do_on_load);
$(window).bind('page:change', do_on_load);
function formContent () {
  document.getElementById("asset_description").value =  document.getElementById("editDescription").innerHTML;
  document.getElementById("asset_title").value =  document.getElementById("editTitle").innerHTML;
  return true;
}
