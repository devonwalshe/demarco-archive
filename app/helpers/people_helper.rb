# == Schema Information
#
# Table name: people
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  nationality    :string(255)
#  dob            :datetime
#  role           :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  bio            :text
#  front_image_id :integer
#

module PeopleHelper
end
