module ApplicationHelper

  ## DRY helper for models and routes rewrite
  ## This function allows urls to be based on specified model attributes, but linked to an id
  ## so the urls can change without breaking internal or external links.
  def url_rewrite(model, params, flash)

    ## if the model has_url, dynamically route with its url attribute
    if model.instance_methods.include?(:url)

      ## split the id from the url string
      if params[:url]
        url_with_id = params[:url]
        id = get_id(url_with_id)
      else
        id = params[:id]
      end

      ## generic instance variable for routing
      iv = model.find(id)

      ## provide the expected model instance variable for the views
      instance_variable_set "@#{model.to_s.downcase}", iv


      ## reroute if the url parameter doesn't match, with the action if its not show
      if iv.url != url_with_id
        if params[:action] == "show"
          redirect_to "/#{model.to_s.downcase.pluralize}/#{iv.url}"
        else
          redirect_to "/#{model.to_s.downcase.pluralize}/#{iv.url}/#{params[:action]}"
        end
      end

    ## if the model doesn't has_url, apply the normal route
    else
      instance_variable_set "@#{model.to_s.downcase}", model.find(params[:id])
    end
  end


  def get_id(url)
    id = url.split('-').first
  end

  ## List of links for views

  def comma_seperated_links_for(list)

    raise TypeError, "parameter must be an array" unless list.is_a? Array
    return if list.count == 0

    list.collect do |item|
      raise TypeError, "items must respond to 'name'" unless last_item.respond_to? :name
      link_to(item.name, url_for(item))
     end.join(", ").html_safe
  end

  def show_errors(object, field_name)
    if object.errors.any?
      if !object.errors.messages[field_name].blank?
        object.errors.messages[field_name].join(". ")
      end
    end
  end

end
