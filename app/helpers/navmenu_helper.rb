module NavmenuHelper

  def ishome
    if params["action"] == "index" && params["controller"] == "home"
      @ishome = true
    else
      @ishome = false
    end
  end

  def navigate_back
    if params["action"] == "show" && params["controller"] == "assets"
      @back = true
    elsif params["action"] == "update" && params["controller"] == "assets"
      @back = true
    else
      @back = false
    end
  end

  def editing
    if params["action"] == "edit" && params["controller"] == "assets"
      @editing = true
    else
      @editing = false
    end
  end

  def newasset
    if params["action"] == "new" && params["controller"] == "assets"
      @newasset = true
    else
      @newasset = false
    end
  end

  def addasset
    if current_user && current_user.admin? == true
      @addasset = true
    else
      @addasset = false
    end
  end

  def newperson
    if params["action"] == "new" && params["controller"] == "people"
      @newperson = true
    else
      @newperson = false
    end
  end

  def editperson
    if params["action"] == "edit" && params["controller"] == "people"
      @editperson = true
    else
      @editperson = false
    end
  end

  def newcol
    if params["action"] == "new" && params["controller"] == "collections"
      @newcol = true
    else
      @newcol = false
    end
  end

  def editcol
    if params["action"] == "edit" && params["controller"] == "collections"
      @editcol = true
    else
      @editcol = false
    end
  end



  def dashboard
    if params["action"] == "dashboard" && params["controller"] == "users"
      @dashview = true
      @dashgo = false
    else
      @dashview = false
      @dashgo = true
    end
  end

  def favorites
    if params["action"] == "favorites" && params["controller"] == "users"
      @favorites = true
    else
      @favorites = false
    end
  end

  def save_to_collection
    if params["controller"] == "assets"
      @save = true
    else
      @save = false
    end
  end

  def new_collection
    if params["action"] == "index" && params["controller"] == "collections"
      @collection = true
    else
      @collection = false
    end
  end

  def view_collections
    @viewcol = true
    if params["action"] == "index" && params[:controller] == "collections"
      @viewcol = false
    end
    @archive_collection = Collection.find_or_create_by(title: "Demarco Archive Collections")
  end

  def show_collection
    if params["action"] == "show" && params[:controller] == "collections"
      @showingcollection = true
    else
      @showingcollection = false
    end
  end

  def edit_asset
    if params[:action] == "show" && params[:controller] == "assets"
      @edit = true
    elsif params[:action] == "update" && params[:controller] == "assets"
      @edit = true
    else
      @edit = false
    end
  end
  
  def about
    unless params[:action] == "about"
      @about = true
    else
      @about = false
    end
  end
  
  # Need to figure out if this is image analytics or user usage
  def usage_stats
    if params["action"] == "show" && params["controller"] == "assets"
      @usage = true
    elsif logged_in?
      @usage = true
    else
      @usage = false
    end
  end

  def search
    if params["action"] == "search" && params["controller"] == "search"
      @search = true
    else
      @search = false
    end
  end

  def set_nav_states
    if logged_in? && current_user.admin?
      save_to_collection
      usage_stats
      dashboard
      new_collection
      dashboard
      view_collections
      search
      newasset
      newperson
      editperson
      newcol
      editcol
      show_collection
      favorites
      edit_asset
      addasset
      editing
    elsif logged_in? 
      save_to_collection
      usage_stats
      dashboard
      new_collection
      dashboard
      view_collections
      search
      newasset
      newperson
      editperson
      newcol
      editcol
      show_collection
      favorites
    else
      new_collection
      view_collections
      show_collection
    end
    ishome
    navigate_back
    about
  end

  def nav_menu
    set_nav_states

    @nav_state = { ishome: @ishome, back: @back, save: @save, edit: @edit, usage: @usage, dash: @dash, collection: @collection, dashview: @dashview, dashgo: @dashgo, viewcol: @viewcol, search: @search, editing: @editing, newasset: @newasset, newperson: @newperson, editperson: @editperson, newcol: @newcol, editcol: @editcol, showingcollection: @showingcollection, addasset: @addasset, favorites: @favorites, about: @about}
  end
end
