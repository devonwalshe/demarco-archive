module SessionsHelper
  
  def log_in(user)
    # remember_token = User.new_remember_token
    # cookies.permanent[:remember_token] = remember_token
    # user.update_attribute(:remember_token, User.digest(remember_token))
    # self.current_user = user        
    session[:user_id] = user.id
  end
  
  def log_out
    if logged_in?
      forget(current_user)
      session.delete(:user_id)
      @current_user = nil
      # current_user.update_attribute(:remember_token, User.digest(User.new_remember_token))
      # cookies.delete(:remember_token)
      # self.current_user = nil
    else
      
    end
  end
  
  # def current_user=(user)
  #   @current_user = user
  # end
  
  def current_user
    # remember_token = User.digest(cookies[:remember_token])
    # @current_user ||= User.find_by(remember_token: remember_token)
    
    # New version
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end
  
  def logged_in?
      !current_user.nil?
  end
  
  def remember(user)
    # Remembers a user in a persistant session
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token  
  end

  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

end
