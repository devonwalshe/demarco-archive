class ApplicationMailer < ActionMailer::Base
  default from: "hello@isaacgc.com"
  layout 'mailer'
end
