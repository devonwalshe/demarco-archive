class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, 
         from: "web-administrator@dundee.ac.uk",
         subject: "Demarco Archive Account Activation",
         reply_to: "web-administrator@dundee.ac.uk"      
  end

  
  def password_reset(user)
    @user = user
    mail to: user.email, 
         from: "web-administrator@dundee.ac.uk",
         subject: "Demarco Archive Password Reset",
         reply_to: "web-administrator@dundee.ac.uk"
  end
end
