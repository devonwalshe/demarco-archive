class ContactMailer < ApplicationMailer
  
  def send_contact_message(contact_message)
    @contact_message = contact_message
    mail to: "demarcoarchive@dundee.ac.uk",
         from: "web-administrator@dundee.ac.uk",
         subject: "Demarco Digital Archive Contact Form - Sender: #{@contact_message[:contact_name]}",
         reply_to: "demarcoarchive@dundee.ac.uk"
  end
  
  
end