#!/bin/bash
cd /home/demarco/www/demarco-archive
command="$(pumactl status)"

if [[ $command =~ "Puma is started" ]]; then
  echo "Process is running."
else
  echo "process is not running"
  rvmsudo /home/demarco/.rvm/gems/ruby-2.2.1@demarco/bin/puma -p 80
fi
