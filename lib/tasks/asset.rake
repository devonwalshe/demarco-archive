### Asset tasks

namespace :asset do
  
  task :strip_title => :environment do 
    assets = Asset.all
    assets.each do |asset|
      puts "stripping =>" + asset.id.to_s
      asset.title = ActionView::Base.full_sanitizer.sanitize(asset.title)
      asset.save
    end
     
  end
  
  task :untitle_nil_assets => :environment do
    nil_assets = Asset.where(title: nil)
    nil_assets.each do |asset|
      asset.title = "Untitled"
      asset.save
    end
  end
  
  task :format_description => :environment do
    # assets = Asset.find(:all)
    #   puts "stuff"
    #   # puts "stripping from description =>" + asset.id.to_s
    #   # asset.title = ActionView::Base.full_sanitizer.sanitize(asset.title)
    #   # asset.save   
  end
  
  task :add_status => :environment do
    #Finds untitled assets and sets them to unpublished, and published otherwise
    
    puts "Adding status to assets"
    assets = Asset.all
    progressbar = ProgressBar.create(:title => "Adding status'", :starting_at => 0, :total => assets.count, :format => '%a <%B> %p%% %t')
    assets.each do |asset|
      if asset.title == "Untitled"
        asset.status = "Unpublished"
        progressbar.increment
      elsif asset.title != "Untitled"
        asset.status = "Published"
        progressbar.increment
      end
      asset.save
    end
    puts "All done!"
  end
  
  task :add_cached_views => :environment do    
    puts "Adding cached_views to assets"
    assets = Asset.all
    progressbar = ProgressBar.create(:title => "Adding cached_views'", :starting_at => 0, :total => assets.count, :format => '%a <%B> %p%% %t')
    assets.each do |asset|
      views = asset.asset_views.count
      asset.cached_views = views
      progressbar.increment
    asset.save
    end
    puts "All done!"
  end
  
end