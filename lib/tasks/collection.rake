namespace :collection do

  task :convert_to_archive_collections => :environment do
    @archive_collection = Collection.find_or_create_by(title: "Demarco Archive Collections")

    @site_collection_names = [
      "Exhibition",
      "Places",
      "Event",
      "Publication",
      "Institution",
      "Artwork",
      "Edinburgh Arts",
      "Theatre",
      "Country",
      "Edinburgh Festival"
    ]

    @current_site_collections = Collection.where("title IN (?)", @site_collection_names)


    progressbar = ProgressBar.create(:title => "Converting site collections to archive collections", :starting_at => 0, :total => @current_site_collections.count, :format => '%a <%B> %p%% %t')

    @current_site_collections.each do |collection|
      collection.parent_id = @archive_collection.id
      collection.save
      progressbar.increment
    end

  end

end
