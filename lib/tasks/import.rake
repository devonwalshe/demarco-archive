## Import tasks for migrating and setting up new data in the application from the original database

## Set rails root
root = Rails.root

## Requirements
require 'csv'
require 'json'

## csv_import.rb gives us 'read_csv' which reads all the files and converts them to dictionaries - the keys available are:
  # :subject_criteria - subjects for images, more semantic then categoric
  # :main_criteria - sub criteria from criteria
  # :criteria - 10 criteria types 
  # :mappings - main file mapping categories to images
  # :images - base image list
  # :acquisition_source - linked original asset source
  # :medium
  # :resolution
  # :bitdepth
  # :people
  # :roles
  # :nationality
  # :people_mappings - custom dict mapping people to images


require "#{root}/lib/shared_functions/csv_import"

namespace :import do
  
    
  desc "Reads CSV file, finds images and asks Amazon from chs, downloads them and then saves a new file"
  task :assets, [:from, :to] => :environment do |t, args|
    
    ## Get all original db data from csv files
    files_hash = read_csv_files
    assets = files_hash[:images]
    
    ## Setup Progress Bar
    progressbar = ProgressBar.create(:title => "Importing Assets", :starting_at => 0, :total => args[:to].to_i-args[:from].to_i, :format => '%a <%B> %p%% %t')
    
    ## Iterate through assets
    assets.values[args[:from].to_i..args[:to].to_i].each do |row|
       
       if Asset.where(:file_file_name => row[:largeimage]).first == nil
         begin
           
           progressbar.log "Adding and uploading #{row[:largeimage]} into the database and s3"
           a = Asset.new
           remote = open("http://demarco-load.s3.amazonaws.com/#{row[:largeimage]}")
           def remote.original_filename;base_uri.path.split('/').last; end
           a.file = remote
           
           a.title = row[:title]
           a.archive_original_code = row[:existencelocationoriginals]
           a.acquisition_source = files_hash[:acquisition_source][row[:aquisitionid].to_i][:name]
           a.description = row[:scopecontent] 
           a.medium = files_hash[:medium][row[:mediumid].to_i][:name]
           a.original_scan_resolution = files_hash[:resolution][row[:imageresolution].to_i][:name] 
           a.bit_depth = files_hash[:bitdepth][row[:bitdepthid].to_i][:name]
           a.date_from = Date.strptime(row[:datefrom],"%m/%d/%Y")
           a.date_to = Date.strptime(row[:dateto],"%m/%d/%Y")
           a.deleted = row[:deleted].match(/(true|t|yes|y|1)$/i) != nil
           a.archive_reference = row[:referencecodes]   
           a.save
           progressbar.increment
         rescue => error
           progressbar.increment
           progressbar.log "Exception Raised:" + " #{row[:largeimage]} " + error.message
         end
       else
          progressbar.increment
          progressbar.log "Asset #{row[:largeimage]} already in the database. Skipping ..."
          
       end
             
    end
  end
  
  task :tags => :environment do

    
    ## Set up data
    files_hash = read_csv_files
    assets = files_hash[:images]
    mappings = files_hash[:mappings]
    
    ## Reduce mappings and images to whats in the app db
    db_assets = Asset.all
    assets = assets.select{|k, v| db_assets.collect{|image| image.file_file_name}.include? v[:largeimage]}
    mappings = mappings.select{|i,d| assets.collect{|k, v| k}.include? d[:baseid]}
    print "got here"
    
    ## Setup Progress Bar
    progressbar = ProgressBar.create(:title => "Importing Tags", :starting_at => 0, :total => mappings.count, :format => '%a <%B> %p%% %t')
    
    ## Iterate over mappings and add tags and taggings for each one
    mappings.each do |k, v|
        
      base_id = v[:baseid]
      
      ## Iterate over each mapping row three times and link the id to the related object
      [[:criteria, :criteriaid], [:main_criteria, :maincriteriaid], [:subject_criteria, :subjectcriteriaid]].each do |field_name|   
        if Asset.where(:file_file_name => assets[base_id][:largeimage]).first
          begin
            mapping_id = v[field_name[1]]
            if Tag.where(:name => files_hash[field_name[0]][mapping_id][:name]).first == nil 
              t = Tag.new
              t.name = files_hash[field_name[0]][mapping_id][:name]
              t.save
            else
              t = Tag.where(:name => files_hash[field_name[0]][mapping_id][:name]).first
            end
          
            tg = Tagging.new
            tg.asset_id = Asset.where(:file_file_name => assets[base_id][:largeimage]).first.id
            progressbar.log "Tag:" + Tag.find(t.id).name + " -- Image: " + Asset.where(:file_file_name => assets[base_id][:largeimage]).first.file_file_name
            tg.tag_id = t.id
            tg.save
          rescue ActiveRecord::RecordNotFound => error
            puts "Exception Raised on #{t.name}: " + error.message
          end
        end 
      end
      progressbar.increment    
    end  
  end
  
  desc "Imports collections using criteria and main criteria"
  task :collections => :environment do
    
    ## Init objects
    files_hash = read_csv_files
    assets = files_hash[:images]
    criterias = files_hash[:criteria]
    main_criterias = files_hash[:main_criteria]
    mappings = files_hash[:mappings]
    
    ### Reduce assets and mappings by items in the database
    db_assets = Asset.all
    puts "shrinking assets and mappings to images in the database"
    assets = assets.select{|k, v| db_assets.collect{|image| image.file_file_name}.include? v[:largeimage]}
    mappings = mappings.select{|i,d| assets.collect{|k, v| k}.include? d[:baseid]}
    
    ## Setup Progress Bar
    progressbar = ProgressBar.create(:title => "Importing Collections...", :starting_at => 0, :total => mappings.count, :format => '%a <%B> %p%% %t')
    
    ## Iterate through assets
    mappings.each do |k, v|
      
      # Find_or_create criteria as a collection
      collection = Collection.create_with(:public => true, :user_id => 0, :front_image_id => Asset.where(file_file_name: assets[v[:baseid]][:largeimage]).first.id).find_or_create_by(:title => criterias[v[:criteriaid]][:name])
      
      
      # Find_or_create main_criteria as a collection and add the criteria as its parents
      child_collection = Collection.create_with(parent_id: collection.id, :public => true, :user_id => 1, :front_image_id => Asset.where(file_file_name: assets[v[:baseid]][:largeimage]).first.id).find_or_create_by(:title => main_criterias[v[:maincriteriaid]][:name])
      
      
      # Step 3 add asset to main_criteria collection if it exists 
      if Asset.where(file_file_name: assets[v[:baseid]][:largeimage]).exists?
        
        
        asset = Asset.where(file_file_name: assets[v[:baseid]][:largeimage]).first
        
        
        collection_item = CollectionItem.new
        collection_item.collection_id = child_collection.id
        collection_item.asset_id = asset.id
        collection_item.save
        
      end
      ##
      progressbar.increment
    end
    
      
  end
  
  desc "Import people and their roles"
  task :people => :environment do
    ## Set up data
    files_hash = read_csv_files
    assets = files_hash[:images]
    people_mappings = files_hash[:people_mappings]
    people = files_hash[:people]
    nationality = files_hash[:nationality]
    roles = files_hash[:roles]
        
    ## Reduce mappings and images to whats in the app db
    
    db_assets = Asset.all
    assets = assets.select{|k, v| db_assets.collect{|image| image.file_file_name}.include? v[:largeimage]}
    people_mappings = people_mappings.select{|i,d| assets.collect{|k, v| k}.include? d[:baseid]}
    
    ## Setup Progress Bar
    progressbar = ProgressBar.create(:title => "Importing People", :starting_at => 0, :total => people_mappings.count, :format => '%a <%B> %p%% %t')

    ## Iterate and save the data
    people_mappings.each do |k, v|
        
      base_id = v[:baseid]
      
      ## Iterate over each mapping row three times and link the id to the related object   
      if Asset.where(:file_file_name => assets[base_id][:largeimage]).first
      
        if Person.where(:name => people[v[:personid]][:name]).first == nil 
          p = Person.new
          p.name = people[v[:personid]][:name]
          p.nationality = nationality[people[v[:personid]][:nationalityid]][:name]
          p.role = roles[v[:role]][:name] 
          p.save
        else
          p = Person.where(:name => people[v[:personid]][:name]).first
        end
        
        ap = AssetPerson.new
        ap.asset_id = Asset.where(:file_file_name => assets[base_id][:largeimage]).first.id
        ap.person_id = p.id
        
        if p.id
          progressbar.log "Person:" + Person.find(p.id).name + " -- Image: " + Asset.where(:file_file_name => assets[base_id][:largeimage]).first.file_file_name 
        end
        ap.save
      
      end
      progressbar.increment    
    end
    
  end
 
  desc "Import colour"
  task :colour => :environment do
    
    
    ## Set up data
    files_hash = read_csv_files
    assets = files_hash[:images]
    colour = files_hash[:colour]
    
    ## Reduce mappings and images to whats in the app db
    db_assets = Asset.all
    
    ## Setup Progress Bar
    progressbar = ProgressBar.create(:title => "Importing Colour", :starting_at => 0, :total => colour.count, :format => '%a <%B> %p%% %t')
    
    colour.each do |k, v|
      if db_assets.where(:file_file_name => v[:largeimage]).first
        db_asset = db_assets.where(:file_file_name => v[:largeimage]).first
        
        if v[:colour] == 0
          db_asset.colour = false
        elsif v[:colour] == 1
          db_asset.colour = true
        end
        db_asset.save
      end
      progressbar.increment 
    end
    
       
  end
  
  desc "Import people-image"
  task :people_images => :environment do
    
    
    ## Set up data
    @people = Person.all
    
    ## Setup Progress Bar
    progressbar = ProgressBar.create(:title => "Importing People Images", :starting_at => 0, :total => @people.count, :format => '%a <%B> %p%% %t')
    
    @people.each do |person|
      person.front_image_id = person.assets.first.id
      person.save
      
      progressbar.increment 
    end
    
       
  end
    
end
   
