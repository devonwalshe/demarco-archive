namespace :reset do
  
  desc "resets the assets table"
  task :assets => :environment do
    puts "Clearing out Asset table"
    progressbar = ProgressBar.create(:title => "Destroying Assets", :starting_at => 0, :total => Asset.all.count, :format => '%a <%B> %p%% %t')
    Asset.all.each do |asset|
      asset.destroy
      progressbar.increment
      progressbar.log "Destroying #{asset.file_file_name}, id: #{asset.id}"
    end
    puts "Finished"
    ActiveRecord::Base.connection.reset_pk_sequence!('assets')
  end
  
  task :tags => :environment do
    puts "Clearing out Tags table"
    progressbar = ProgressBar.create(:title => "Destroying Tags", :starting_at => 0, :total => Tag.all.count, :format => '%a <%B> %p%% %t')
    Tag.all.each do |tag|
      tag.destroy
      progressbar.increment
      progressbar.log "Destroying #{tag.name}, id: #{tag.id}"
    end
    puts "Finished"
    ActiveRecord::Base.connection.reset_pk_sequence!('tags')
    ActiveRecord::Base.connection.reset_pk_sequence!('taggings')
  end
  
  task :collections => :environment do
    puts "Clearing out Collections table"
    progressbar = ProgressBar.create(:title => "Destroying Collections", :starting_at => 0, :total => Collection.all.count, :format => '%a <%B> %p%% %t')
    Collection.all.each do |collection|
      collection.destroy
      progressbar.increment
      progressbar.log "Destroying #{collection.title}, id: #{collection.id}"
    end
    puts "Finished"
    ActiveRecord::Base.connection.reset_pk_sequence!('collections')
    ActiveRecord::Base.connection.reset_pk_sequence!('collection_items')
  end
  
  task :people => :environment do
    puts "Clearing out People tables"
    progressbar = ProgressBar.create(:title => "Destroying People", :starting_at => 0, :total => Person.all.count, :format => '%a <%B> %p%% %t')
    Person.all.each do |p|
      p.destroy
      progressbar.increment
      progressbar.log "Destroying #{p.name}, id: #{p.id}"
    end
    puts "Finished"
    ActiveRecord::Base.connection.reset_pk_sequence!('people')
    ActiveRecord::Base.connection.reset_pk_sequence!('asset_people')
  end
  
  
end