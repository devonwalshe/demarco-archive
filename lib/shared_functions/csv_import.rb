## Reads csv files from the directory where they are stored

def read_csv_files 
  
  ## Set up Root
  root = Rails.root
  
  ## Set up empty hash with info
  demarco_db_data = {}
  
  puts "Reading data from original demarco database..."

  images = {}
  CSV.foreach("#{root}/lib/import_files/base.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    images[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]
  end 
  
  mappings = {}
  CSV.foreach("#{root}/lib/import_files/basecriteriacomposite.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    mappings[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]
  end
  # 
  # #Only includes mappings entries that are in the sliced assets file
  # ids = JSON.parse(File.read("#{root}/lib/import_files/ids"))
  # mp = {}
  # mappings.each do |k, v|
  #   if ids.include?(v[:baseid])
  #     mp[k] = v
  #   end
  # end 
  
  criteria = {}
  
  CSV.foreach("#{root}/lib/import_files/criteria.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    criteria[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]
  end
  
  
  main_criteria = {}
  CSV.foreach("#{root}/lib/import_files/maincriteria.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    main_criteria[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]
  end

  subject_criteria = {}
  CSV.foreach("#{root}/lib/import_files/subjectcriteria.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    subject_criteria[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]
  end 
  
  acquisition_source = {}
  CSV.foreach("#{root}/lib/import_files/sourceofacquisition.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    acquisition_source[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]
  end
  
  medium = {}
  CSV.foreach("#{root}/lib/import_files/medium.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    medium[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]
  end
   
  resolution = {}
  CSV.foreach("#{root}/lib/import_files/imageresolution.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    resolution[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]      
  end  
  
  bitdepth = {}
  CSV.foreach("#{root}/lib/import_files/bitdepth.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    bitdepth[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]      
  end
  
  people = {}
  CSV.foreach("#{root}/lib/import_files/person.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    people[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]      
  end
  
  roles = {}
  CSV.foreach("#{root}/lib/import_files/personrole.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    roles[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]      
  end
  
  nationality = {}
  CSV.foreach("#{root}/lib/import_files/nationality.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    nationality[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]      
  end
  
  people_mappings = {}
  CSV.foreach("#{root}/lib/import_files/basepersoncomposite.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    people_mappings[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]      
  end
  
  colour = {}
  CSV.foreach("#{root}/lib/import_files/colour.csv", :headers => true, :header_converters => :symbol, :converters => :all) do |row|
    colour[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]      
  end
  
  puts "files loaded"
  
  return demarco_db_data.merge!(:subject_criteria => subject_criteria, :main_criteria => main_criteria, :criteria => criteria, :mappings => mappings, :images => images, :acquisition_source => acquisition_source, :medium => medium, :resolution => resolution, :bitdepth => bitdepth, :people => people, :roles => roles, :nationality => nationality, :people_mappings => people_mappings, :colour => colour)
   
end