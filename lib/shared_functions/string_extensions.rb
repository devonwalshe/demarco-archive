
class String
  
  # Turns a string into a nice, pretty URL
  def pretty_url
    str_src = Iconv.iconv('ascii//ignore//translit', 'utf-8', self.to_s.downcase) rescue self.to_s.downcase
    return  str_src.
            to_s.
            gsub(/(\s+(and|or|the|go|at|be|to|as|at|is|it|an|of|on|a)\s+)+/, " ").
            gsub(/[^A-Za-z0-9\s]/, "").
            gsub(/\s+/, "_").
            gsub(/\_+/, "_").
            chomp("_").
            split("_")[0,10].
            join("_").downcase
  end
  
end