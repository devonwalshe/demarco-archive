module Url #:nodoc:
  
  def self.included(base)
    base.extend(ClassMethods)
  end
  
        
  module ClassMethods
    
    def has_url(attribute, options={})
                
      cattr_accessor :url_source_attribute, :url_attribute
      
      self.url_source_attribute = options[:generated_from] || "title"
      self.url_attribute = attribute
      
      validates attribute, :presence=>true, :format=>{ :with=>/[a-zA-Z0-9_]+/i }, :unless => Proc.new { self[self.class.url_source_attribute].blank?  }
      
      include Url::InstanceMethods
      
    end
            
  end
  

  module InstanceMethods
            
    def update_url
      self[self.class.url_attribute] = 
        String::pretty_url( 
          self.send(self[self.class.url_source_attribute]).to_s 
        )
    end
    
    def url
      class_attribute = self[self.class.url_source_attribute]
      if class_attribute
        return "#{id}-#{class_attribute.pretty_url}"
      else
        return "#{id}"
      end
    end
    
    
  end
  

  
end

ActiveRecord::Base.send(:include, Url)