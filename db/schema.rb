# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200722222258) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "asset_people", force: :cascade do |t|
    t.integer  "asset_id"
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "asset_views", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "asset_id"
    t.string   "context"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "assets", force: :cascade do |t|
    t.string   "asset_type",               limit: 255
    t.string   "title",                    limit: 255
    t.string   "archive_original_code",    limit: 255
    t.string   "archive_reference",        limit: 255
    t.string   "acquisition_source",       limit: 255
    t.text     "description"
    t.string   "medium",                   limit: 255
    t.string   "original_scan_resolution", limit: 255
    t.string   "bit_depth",                limit: 255
    t.date     "date_from"
    t.date     "date_to"
    t.string   "dimensions",               limit: 255
    t.boolean  "deleted"
    t.string   "file_file_name",           limit: 255
    t.string   "file_content_type",        limit: 255
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "colour"
    t.string   "status"
    t.integer  "cached_views"
    t.boolean  "video"
    t.string   "video_url"
    t.string   "thumbnail_url"
  end

  create_table "collection_items", force: :cascade do |t|
    t.integer  "collection_id"
    t.integer  "asset_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "collections", force: :cascade do |t|
    t.string   "title",                  limit: 255
    t.integer  "user_id"
    t.boolean  "public"
    t.integer  "front_image_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "collection_items_count",             default: 0
    t.integer  "parent_id"
    t.boolean  "featured"
  end

  add_index "collections", ["parent_id"], name: "index_collections_on_parent_id", using: :btree

  create_table "favorites", force: :cascade do |t|
    t.integer  "asset_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "people", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "nationality",    limit: 255
    t.datetime "dob"
    t.string   "role",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "bio"
    t.integer  "front_image_id"
  end

  create_table "static_texts", force: :cascade do |t|
    t.text     "front_header"
    t.text     "front_introduction"
    t.text     "front_quote"
    t.text     "about_title"
    t.text     "about_body"
    t.text     "about_copyright"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "asset_id"
    t.integer  "tag_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.string   "email",             limit: 255
    t.string   "remember_digest",   limit: 255
    t.string   "password_digest",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin"
    t.string   "activation_digest"
    t.boolean  "activated",                     default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["remember_digest"], name: "index_users_on_remember_digest", using: :btree

end
