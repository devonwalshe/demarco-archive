class AddCachedViewsToAsset < ActiveRecord::Migration
  def change
    add_column :assets, :cached_views, :integer
  end
end
