class AddThumbnailToAsset < ActiveRecord::Migration
  def change
    add_column :assets, :thumbnail_url, :string
  end
end
