class CreateCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|

      t.string :title
      t.integer :user_id
      t.boolean :public
      t.integer :front_image_id
      t.text :description

      t.timestamps
    end
  end
end
