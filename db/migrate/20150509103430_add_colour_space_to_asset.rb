class AddColourSpaceToAsset < ActiveRecord::Migration
  def change
    add_column :assets, :colour, :boolean
  end
end
