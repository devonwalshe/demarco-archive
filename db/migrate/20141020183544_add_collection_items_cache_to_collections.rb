class AddCollectionItemsCacheToCollections < ActiveRecord::Migration
  def change
    
    add_column :collections, :collection_items_count, :integer, default: 0
  end
end
