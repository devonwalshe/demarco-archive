class CreateAssetViews < ActiveRecord::Migration
  def change
    create_table :asset_views do |t|
      t.integer :user_id
      t.integer :asset_id
      t.string :context
      t.timestamps null: false
    end
  end
end
