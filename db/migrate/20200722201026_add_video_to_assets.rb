class AddVideoToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :video, :boolean
    add_column :assets, :video_url, :string
  end
end
