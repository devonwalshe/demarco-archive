class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name
      t.string :nationality
      t.datetime :dob
      t.string :role

      t.timestamps
    end
  end
end
