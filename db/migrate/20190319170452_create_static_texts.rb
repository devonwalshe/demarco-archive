class CreateStaticTexts < ActiveRecord::Migration
  def change
    create_table :static_texts do |t|
      t.text :front_header
      t.text :front_introduction
      t.text :front_quote
      t.text :about_title
      t.text :about_body
      t.text :about_copyright
      t.timestamps null: false
    end
  end
end
