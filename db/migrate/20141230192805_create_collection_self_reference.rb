class CreateCollectionSelfReference < ActiveRecord::Migration
  def change
    add_reference :collections, :parent, index: true

  end
end
