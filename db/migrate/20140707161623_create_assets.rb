class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string  :asset_type
      t.string :title
      t.string :archive_original_code
      t.string :archive_reference 
      t.string :acquisition_source 
      t.text :description
      t.string :medium
      t.string :original_scan_resolution 
      t.string :bit_depth
      t.date :date_from
      t.date :date_to
      t.string :dimensions
      t.boolean :deleted
      t.attachment :file

      t.timestamps
    end
  end
end
