class CreateAssetPeople < ActiveRecord::Migration
  def change
    create_table :asset_people do |t|
      t.integer :asset_id
      t.integer :person_id

      t.timestamps
    end
  end
end
