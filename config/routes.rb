DemarcoArchive::Application.routes.draw do

  get 'collection_items/create'
  get 'collection_items/delete'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  ## Taggings
  delete '/taggings' => 'taggings#destroy'
  post '/taggings' => 'taggings#create'
  get '/taggings' => 'taggings#show'

  ## Password Resets
  get 'password_resets/new'
  get 'password_resets/edit'

  ## Sessions
  match '/signup',  to: 'users#new',            via: 'get'
  match '/signin',  to: 'sessions#new',         via: 'get'
  match '/signout', to: 'sessions#destroy',     via: 'delete'

  ## Assets
  get '/assets/new' => 'assets#new', as: :new_asset
  get '/assets/:url' => 'assets#show', as: :asset
  get '/assets' => 'assets#index', as: :assets
  patch '/assets/:url' => 'assets#update'
  get '/assets/:url/edit' => 'assets#edit', as: :edit_asset
  post '/assets/' => 'assets#create'
  get '/assets/:url/lightbox' => 'assets#lightbox', as: :asset_lightbox
  delete '/assets/:url' => 'assets#delete', as: :delete_asset

  ## People
  get '/people' => 'people#index'
  get '/people/new' => 'people#new', as: :new_person
  get '/people/:url' => 'people#show', as: :person
  patch '/people/:url' => 'people#update'
  get '/people/:url/edit' => 'people#edit', as: :edit_person
  post '/people' => 'people#create'
  delete '/people/:url' => 'people#destroy', as: :delete_person

  ## Asset People
  delete 'asset_people' => 'asset_people#destroy'
  post 'asset_people' => 'asset_people#create'

  ## Collections
  get '/collections/new' => 'collections#new', as: :new_collection
  get '/collections/:url' => 'collections#show'
  get '/collections/:url/list' => 'collections#show', as: :collection_list
  get '/collections' => 'collections#index'
  patch '/collections/:url' => 'collections#update'
  get '/collections/:url/edit' => 'collections#edit', as: :edit_collection
  get '/collections/public_collections' => 'collections#public_collections', as: :public_collections
  delete '/collections/:url' => 'collections#destroy'
  resources :collections

  ## Collection Items
  post '/collection_items/' => 'collection_items#create'
  delete '/collection_items/' => 'collection_items#delete'

  ## Tags
  get 'tags/new' => 'tags#new', as: :new_tag
  get '/tags/:url' => 'tags#show'
  get '/tags' => 'tags#index'
  patch '/tags/:url' => 'tags#update'
  get '/tags/:url/edit' => 'tags#edit', as: :edit_tag
  post '/tags' => 'tags#create'

  ## Search
  get '/search' => 'search#search', as: :search
  post '/search' => 'search#search'

  ## Static text
  get '/static_text/edit' => 'static_text#edit'
  patch '/static_text' => 'static_text#update'

  ## User Sessions
  resources :sessions, only: [:new, :create, :destroy]

  ## Users
  resources :users
  get '/users/:url/history' => 'users#history', as: :history
  get '/users/:url/favourites' => 'users#favorites', as: :favorites
  get '/users/:url/dashboard' => 'users#dashboard', as: :dashboard
  post '/users/:url/toggle_admin' => 'users#toggle_admin'

  ### Not used for now
  # get '/users/:url/collections' => 'users#collections', as: :user_collections

  ## Favourites
  resources :favorites, only: [:create, :destroy]

  # Account activations and reset
  resources :account_activations, only: [:edit]
  resources :password_resets, only: [:new, :create, :edit, :update]

  # Error pages
  %w( 404 422 500 503 ).each do |code|
    get code, :to => "home#errors", :code => code
  end

  ## Static
  get '/about' => 'home#about'

  ## contact form
  post '/home/send_contact' => 'home#send_contact'

  ## Root
  root 'home#index'

end
