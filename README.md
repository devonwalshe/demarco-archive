# README #

Private repo for the Demarco Archive website

Running on: Ruby 2.2.1 / Rails 4.0.2

Check the gemfile on master branch for a full list of dependencies

### How do I get set up? ###

pull the repo
`git clone git@bitbucket.org:devonwalshe/demarco-archive.git`

Setup the database in your rails supported database of choice, configure the database.yml file with your credentials, then: 
`rake db:migrate`

Import data from the original archive:

* `rake import:assets`   
* if this doesn't work, add arguments for a slice of the full database - `rake import:assets[0,500]`  
* `rake import:tags`  
* `rake import:collections`  
* `rake import:people`  
* `rake import:colour`

You can remove these items with  
`rake reset:[any of the above]`

### Who do I talk to? ###

devon.walshe@gmail.com, isaacgc.gmail.com