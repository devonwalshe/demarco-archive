# == Schema Information
#
# Table name: collections
#
#  id                     :integer          not null, primary key
#  title                  :string(255)
#  user_id                :integer
#  public                 :boolean
#  front_image_id         :integer
#  description            :text
#  created_at             :datetime
#  updated_at             :datetime
#  collection_items_count :integer          default(0)
#  parent_id              :integer
#  featured               :boolean
#
# Indexes
#
#  index_collections_on_parent_id  (parent_id)
#

require 'test_helper'

class CollectionsControllerTest < ActionController::TestCase
  setup do
    @collection = collections(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:collections)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create collection" do
    assert_difference('Collection.count') do
      post :create, collection: { description: @collection.description, front_image_id: @collection.front_image_id, public: @collection.public, title: @collection.title, user_id: @collection.user_id }
    end

    assert_redirected_to collection_path(assigns(:collection))
  end

  test "should show collection" do
    get :show, id: @collection
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @collection
    assert_response :success
  end

  test "should update collection" do
    patch :update, id: @collection, collection: { description: @collection.description, front_image_id: @collection.front_image_id, public: @collection.public, title: @collection.title, user_id: @collection.user_id }
    assert_redirected_to collection_path(assigns(:collection))
  end

  test "should destroy collection" do
    assert_difference('Collection.count', -1) do
      delete :destroy, id: @collection
    end

    assert_redirected_to collections_path
  end
end
