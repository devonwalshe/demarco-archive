# == Schema Information
#
# Table name: collection_items
#
#  id            :integer          not null, primary key
#  collection_id :integer
#  asset_id      :integer
#  created_at    :datetime
#  updated_at    :datetime
#

require 'test_helper'

class CollectionItemsControllerTest < ActionController::TestCase
  test "should get create" do
    get :create
    assert_response :success
  end

  test "should get delete" do
    get :delete
    assert_response :success
  end

end
