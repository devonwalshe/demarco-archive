require 'test_helper'

class AssetTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end

# == Schema Information
#
# Table name: assets
#
#  id                       :integer          not null, primary key
#  asset_type               :string(255)
#  title                    :string(255)
#  archive_original_code    :string(255)
#  archive_reference        :string(255)
#  acquisition_source       :string(255)
#  description              :text
#  medium                   :string(255)
#  original_scan_resolution :string(255)
#  bit_depth                :string(255)
#  date_from                :date
#  date_to                  :date
#  dimensions               :string(255)
#  deleted                  :boolean
#  file_file_name           :string(255)
#  file_content_type        :string(255)
#  file_file_size           :integer
#  file_updated_at          :datetime
#  created_at               :datetime
#  updated_at               :datetime
#  colour                   :boolean
#  status                   :string
#  cached_views             :integer
#  video                    :boolean
#  video_url                :string
#  thumbnail_url            :string
#
