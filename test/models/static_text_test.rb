require 'test_helper'

class StaticTextTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end

# == Schema Information
#
# Table name: static_texts
#
#  id                 :integer          not null, primary key
#  front_header       :text
#  front_introduction :text
#  front_quote        :text
#  about_title        :text
#  about_body         :text
#  about_copyright    :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
