require 'test_helper'

class CollectionTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end

# == Schema Information
#
# Table name: collections
#
#  id                     :integer          not null, primary key
#  title                  :string(255)
#  user_id                :integer
#  public                 :boolean
#  front_image_id         :integer
#  description            :text
#  created_at             :datetime
#  updated_at             :datetime
#  collection_items_count :integer          default(0)
#  parent_id              :integer
#  featured               :boolean
#
# Indexes
#
#  index_collections_on_parent_id  (parent_id)
#
